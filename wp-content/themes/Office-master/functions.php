<?php
function office_master_theme_support(){
    add_theme_support('title-tag');
    add_theme_support('post-thumbnails');
    add_image_size('slide-img',1500,500,true);
    add_image_size('sidebar-slide-img',265,192,true);
    add_image_size('team-member',100,80,true);
    add_image_size('post-thumb',850,490,true);
    register_nav_menus(array(
        'primary_menu' => 'Primary Menu'
    ));
}
add_action('after_setup_theme','office_master_theme_support');

function office_master_css_js(){
    wp_enqueue_style('google-font-1','//fonts.googleapis.com/css?family=Open+Sans:400,300');
    wp_enqueue_style('google-font-2','//fonts.googleapis.com/css?family=PT+Sans');
    wp_enqueue_style('google-font-3','//fonts.googleapis.com/css?family=Raleway');

    wp_enqueue_style('bootstrap',get_template_directory_uri().'/assets/bootstrap/css/bootstrap.css');
    wp_enqueue_style('font-awesome',get_template_directory_uri().'/assets/css/font-awesome.min.css');
    wp_enqueue_style('theme-style',get_template_directory_uri().'/assets/css/style.css');
    wp_enqueue_style('animated',get_template_directory_uri().'/assets/css/animate.min.css');
    wp_enqueue_style('office-master-main-css',get_stylesheet_uri());


    wp_enqueue_script('jquery');
    wp_enqueue_script('bootstrap-js',get_template_directory_uri().'/assets/bootstrap/js/bootstrap.min.js','jquery',null,true);
    wp_enqueue_script('wow-js',get_template_directory_uri().'/js/wow.min.js','jquery',null,true);
}
add_action('wp_enqueue_scripts','office_master_css_js');

function footer_extra_script(){?>
    <script>
        new WOW().init();
    </script>
<?php }
add_action('wp_footer','footer_extra_script',30);
    function office_master_fallback_menu(){?>
        <ul class="nav navbar-nav pull-right">
            <li class="active">
                <a href="#">Home</a>
            </li>
            <li>
                <a href="#">About</a>
            </li>
            <li>
                <a href="#">Blog</a>
            </li>
            <li>
                <a href="#">Team</a>
            </li>
            <li>
                <a href="#"><span>Contact</span></a>
            </li>
        </ul>
    <?php }
include_once('inc/custom-post.php');
include_once('inc/cmb2-custom-field.php');
require_once('inc/redux-framework-master/redux-framework.php');
require_once('inc/office-master-theme-op.php');
require_once('inc/custom-sortcode.php');


        // Hooks your functions into the correct filters
        function my_add_mce_button() {
            // check user permissions
            if ( !current_user_can( 'edit_posts' ) && !current_user_can( 'edit_pages' ) ) {
                return;
            }
            // check if WYSIWYG is enabled
            if ( 'true' == get_user_option( 'rich_editing' ) ) {
                add_filter( 'mce_external_plugins', 'office_master_tinymce_plugin' );
                add_filter( 'mce_buttons', 'office_master_register_mce_button' );
            }
        }
        add_action('admin_head', 'my_add_mce_button');

        // Declare script for new button
        function office_master_tinymce_plugin( $plugin_array ) {
            $plugin_array['pallab_first_button'] =$plugin_array['pallab_second_button']=get_template_directory_uri() .'/js/office-master-mce-button.js';
            return $plugin_array;
        }

        // Register new button in the editor
        function office_master_register_mce_button( $buttons ) {
            array_push( $buttons, 'pallab_first_button' );
            array_push( $buttons, 'pallab_second_button' );
            return $buttons;
        }
?>
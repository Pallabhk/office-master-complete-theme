jQuery(document).ready( function($) {
    tinymce.PluginManager.add('pallab_first_button', function( editor, url ) {
        editor.addButton('pallab_first_button', {
            text: 'pallab',
            icon: false,
            onclick: function() {
                editor.insertContent('[slider]');
            }
        });
    });
    tinymce.PluginManager.add('pallab_second_button', function( editor, url ) {
        editor.addButton('pallab_second_button', {
            text: 'Akhy',
            icon: false,
            type:'menubutton',
            menu:[
                {
                    text:'Menu First Level',
                    onclick:function(){
                        editor.insertContent('[From First Level]');
                    }
                },
                {
                    text:'Menu Second Level',
                    onclick:function(){
                        editor.insertContent('[From Second Level]');
                    }
                },
                {
                    text:'Menu Third Level',
                    onclick: function() {
                        editor.windowManager.open( {
                            title: 'Insert Pallab Shortcode',
                            body: [
                                {
                                    type: 'textbox',
                                    name: 'firstname',
                                    label: 'First Name',
                                    value: 'Pallab'
                                },
                                {
                                    type: 'textbox',
                                    name: 'address',
                                    label: 'Address',
                                    multiline: true,
                                    minWidth: 300,
                                    minHeight: 100
                                },
                                {
                                    type: 'listbox',
                                    name: 'gender',
                                    label: 'Select your gender',
                                    'values': [
                                        {text: 'Male', value: 'Male'},
                                        {text: 'Female', value: 'Female'},
                                        {text: 'Other', value: 'Other'}
                                    ]
                                }
                            ],
                            onsubmit: function(x) {
                                editor.insertContent('[pallab firstname="'+ x.data.firstname +'" address="'+ x.data.address+'" gender="'+ x.data.gender+'"]');
                            }
                        });
                    }

                }
            ]
        });
    });
})();


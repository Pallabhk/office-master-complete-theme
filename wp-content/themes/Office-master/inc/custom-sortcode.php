<?php
    function my_slider($para,$content){
        $para= shortcode_atts(array(
            'fz' =>'25px',
            'color'=> 'red'
        ),$para); ob_start();?>
        <h2 style="color: <?php echo $para['color'];?>; font-size: <?php echo $para['fz']?>"><?php echo $content;?></h2>
    <?php  return ob_get_clean();}
    add_shortcode('myslider','my_slider');

    function pallab_slider($para,$content){
        $para= shortcode_atts(array(
            'id' => 0

           ),$para); ob_start();?>

        <div id="carousel-example-generic<?php echo $para['id'];?>" class="carousel slide" data-ride="carousel">

            <!-- Wrapper for slides -->
            <div class="carousel-inner">
                <?php

                $pallab_post=null;
                $pallab_post = new WP_Query(array(
                    'post_type'     =>'slider',
                    'posts_per_page' => -1
                ));

                if($pallab_post->have_posts()){
                    $x=0;
                    while($pallab_post->have_posts()){
                        $x++;
                        $pallab_post->the_post();
                        $slider_caption = get_post_meta(get_the_ID(),'_office-master_slider_caption',true); ?>

                        <div class="item <?php if($x==1){echo 'active';}?>">
                            <?php the_post_thumbnail('slide-img');?>

                        </div>
                    <?php }
                }else{
                    echo 'No posts';
                }

                wp_reset_postdata();
                ?>

            </div>
            <!-- Indicators -->
            <ol class="carousel-indicators">
                <?php
                for($i=0;$i<$x;$i++){?>
                    <li data-target="#carousel-example-generic<?php echo $para['id'];?>" data-slide-to="<?php echo $i;?>" class="<?php if(i==0){ echo 'active';}?>"></li>
                <?php }?>


            </ol>
            <!-- Controls -->
            <a class="left carousel-control" href="#carousel-example-generic<?php echo $para['id'];?>" data-slide="prev">
                <span class="glyphicon glyphicon-chevron-left"></span>
            </a>
            <a class="right carousel-control" href="#carousel-example-generic<?php echo $para['id'];?>" data-slide="next">
                <span class="glyphicon glyphicon-chevron-right"></span>
            </a>
        </div>

       <?php  wp_reset_postdata();
        $pallab_slider=null;
        return ob_get_clean();}
add_shortcode('pallabslider','pallab_slider');
?>
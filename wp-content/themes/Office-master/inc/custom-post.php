<?php
function office_master_custom_post(){
    register_post_type('slider', array(
        'labels' => array(
            'name' => 'Main Slider',
            'menu_name' => 'Slider Menu',
            'all_items' => ' All Slider',
            'add_new' => 'Add New Slider',
            'add_new_item' => 'Add new Ebit Slider'
        ),
        'public' => true,
        'supports' => array(
            'title',  'thumbnail', 'revisions', 'custom-fields', 'page-attributes'
        )
    ));
    register_post_type('services',array(
        'labels'=>array(
            'name' =>'Service',
            'menu_name'=>'Service Menu',
            'all_items'=>'All Service',
            'add_new'=>'Add new Service',
            'add_new_item'=>'Add new Service item'
        ),
        'public' =>true,
        'supports' =>array(
            'title','revisions','custom-fields','page-attributes'
        )
    ));
    register_post_type('team',array(
        'labels'=>array(
            'name' =>'Team',
            'menu_name'=>'Team Menu',
            'all_items'=>'All Team Member',
            'add_new'=>'Add new Team Member',
            'add_new_item'=>'Add new Team Member'
        ),
        'public' =>true,
        'supports' =>array(
            'title','revisions','page-attributes','thumbnail'
        )
    ));
    register_taxonomy(
        'team_catagory',
        'team', array(
        'labels' => array(
            'name'        =>'Team Category',
            'add_new_item'=>'Add New Category'
        ),
        'hierarchical'      =>true,
        'show_admin_column' =>true
    ));
    register_taxonomy(
        'team_tag',
        'team', array(
        'labels' => array(
            'name'        =>'Team Tag',
            'add_new_item'=>'Add New Team Tag'
        ),
        'show_admin_column' =>true
    ));


}
add_action('init','office_master_custom_post');
?>
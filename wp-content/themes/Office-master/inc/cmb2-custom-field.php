<?php
/**
 * Get the bootstrap!
 */
if ( file_exists( __DIR__ . '/cmb2/init.php' ) ) {
    require_once __DIR__ . '/cmb2/init.php';
} elseif ( file_exists(  __DIR__ . '/CMB2/init.php' ) ) {
    require_once __DIR__ . '/CMB2/init.php';
}
    add_action('cmb2_admin_init','office_master_cmb2');
    function office_master_cmb2(){
        $pref= '_office-master_';
        $service_item = new_cmb2_box( array(

            'id'            => 'service_metabox',
            'title'         => __( 'service Metabox', 'office_master' ),
            'object_types'  => array( 'services', ), // Post type
            'context'       => 'normal',
            'priority'      => 'high',
            'show_names'    => true
        ) );
        $service_item->add_field( array(
            'name'       => __( 'Service Icon ', 'office_master' ),
            'desc'       => __( 'Write here your service icon' , 'office_master' ),
            'id'         => $pref.'service_icon',
            'type'       => 'text',
            'repeatable' => true

        ) );
        $service_item->add_field( array(
            'name'       => __( 'Service Description ', 'office_master' ),
            'desc'       => __( 'Write here your service Description' , 'office_master' ),
            'id'         => $pref.'service_description',
            'type'       => 'textarea',

        ) );
        $service_item->add_field( array(
            'name'       => __( 'Service Link Url ', 'office_master' ),
            'desc'       => __( 'Write here your service link url' , 'office_master' ),
            'id'         => $pref.'service_link_url',
            'type'       => 'text',

        ) );
        $service_item->add_field( array(
            'name'       => __( 'Service Link Title ', 'office_master' ),
            'desc'       => __( 'Write here your service link title' , 'office_master' ),
            'id'         => $pref.'service_link_title',
            'type'       => 'text',

        ) );
        $service_item->add_field( array(
            'name'       => __( 'Service Animation type ', 'office_master' ),
            'desc'       => __( 'Write here your service animation type' , 'office_master' ),
            'id'         => $pref.'animation_type',
            'type'       => 'text',

        ) );
        $slider_item = new_cmb2_box( array(

            'id'            => 'Slider_metabox',
            'title'         => __( 'Slider Metabox', 'office_master' ),
            'object_types'  => array( 'slider','services','page' ), // Post type
            'context'       => 'normal',
            'priority'      => 'high',
            'show_names'    => true
        ) );
        $slider_item->add_field( array(
            'name'       => __( 'Slider Caption ', 'office_master' ),
            'desc'       => __( 'Write here your Slider Caption' , 'office_master' ),
            'id'         => $pref.'slider_caption',
            'type'       => 'text',

        ) );
        $special_page= new_cmb2_box( array(

            'id'            => 'special_metabox',
            'title'         => __( 'special Metabox', 'office_master' ),
            'object_types'  => array('page' ), // Post type
            'show_on'       =>array(
                'key'=>'id',
                'value'=>'12'
            ),
            'context'       => 'normal',
            'priority'      => 'high',
            'show_names'    => true
        ) );
        $special_page->add_field( array(
            'name'       => __( 'special Caption ', 'office_master' ),
            'desc'       => __( 'Write here your special Caption' , 'office_master' ),
            'id'         => $pref.'special_caption',
            'type'       => 'text',

        ) );
        $team_member = new_cmb2_box( array(

            'id'            => 'Team_metabox',
            'title'         => __( 'Team Metabox', 'office_master' ),
            'object_types'  => array( 'team' ), // Post type
            'context'       => 'normal',
            'priority'      => 'high',
            'show_names'    => true
        ) );
        $team_member->add_field( array(
            'name'       => __( 'Team Member Designation ', 'office_master' ),
            'desc'       => __( 'Write here your Team Member Designation' , 'office_master' ),
            'id'         => $pref.'team_designation',
            'type'       => 'text',

        ) );
        $team_member->add_field( array(
            'name'       => __( 'BlockQuote  Color', 'office_master' ),
            'desc'       => __( 'Write here your BlockQuote  Color Class' , 'office_master' ),
            'id'         => $pref.'block_color',
            'type'       => 'text',

        ) );
        $team_member->add_field( array(
            'name'       => __( 'Animation Type ', 'office_master' ),
            'desc'       => __( 'Write here your Animation Type' , 'office_master' ),
            'id'         => $pref.'animation_type',
            'type'       => 'text',

        ) );
        $post_meta = new_cmb2_box( array(

            'id'            => 'post_metabox',
            'title'         => __( 'post Metabox', 'office_master' ),
            'object_types'  => array( 'post' ), // Post type
            'context'       => 'normal',
            'priority'      => 'high',
            'show_names'    => true
        ) );
        $post_meta->add_field( array(
            'name'       => __( 'Post Icon ', 'office_master' ),
            'desc'       => __( 'Write here your Post Icon class name' , 'office_master' ),
            'id'         => $pref.'post_icon_class',
            'type'       => 'text',

        ) );
        $about_page_group = new_cmb2_box( array(

            'id'            => 'group_page_metabox',
            'title'         => __( 'post Metabox', 'office_master' ),
            'object_types'  => array( 'page' ), // Post type
            'context'       => 'normal',
            'priority'      => 'high',
            'show_names'    => true,
            'show_on'    =>array(
                'key'   =>'id',
                'value' =>'10'
            )
        ) );
        $about_group_para =$about_page_group->add_field( array(
            'name'       => __( 'Groupable field ', 'office_master' ),
            'id'         => $pref.'about_group_meta',
            'type'       => 'group'

        ) );
        $about_page_group->add_group_field( $about_group_para, array(
            'name'       => __( 'Heading ', 'office_master' ),
            'id'         => $pref.'heading',
            'type'       => 'text'

        ) );
        $about_page_group->add_group_field($about_group_para, array(
            'name'       => __( 'About Description', 'office_master' ),
            'id'         => $pref.'about_description',
            'type'       => 'textarea'

        ) );
        $about_page_group->add_group_field($about_group_para, array(
            'name'       => __( 'A tag hash link', 'office_master' ),
            'id'         => $pref.'hash_link',
            'type'       => 'text',
            'repeatable' =>true

        ) );
        $about_page_group->add_group_field($about_group_para, array(
            'name'       => __( 'A tag hash link title', 'office_master' ),
            'id'         => $pref.'hash_link_title',
            'type'       => 'text',
            'repeatable' =>true

        ) );
    }
?>
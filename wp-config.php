<?php
/**
 * The base configuration for WordPress
 *
 * The wp-config.php creation script uses this file during the
 * installation. You don't have to use the web site, you can
 * copy this file to "wp-config.php" and fill in the values.
 *
 * This file contains the following configurations:
 *
 * * MySQL settings
 * * Secret keys
 * * Database table prefix
 * * ABSPATH
 *
 * @link https://codex.wordpress.org/Editing_wp-config.php
 *
 * @package WordPress
 */

// ** MySQL settings - You can get this info from your web host ** //
/** The name of the database for WordPress */
define('DB_NAME', 'office-master');

/** MySQL database username */
define('DB_USER', 'root');

/** MySQL database password */
define('DB_PASSWORD', '');

/** MySQL hostname */
define('DB_HOST', 'localhost');

/** Database Charset to use in creating database tables. */
define('DB_CHARSET', 'utf8mb4');

/** The Database Collate type. Don't change this if in doubt. */
define('DB_COLLATE', '');

/**#@+
 * Authentication Unique Keys and Salts.
 *
 * Change these to different unique phrases!
 * You can generate these using the {@link https://api.wordpress.org/secret-key/1.1/salt/ WordPress.org secret-key service}
 * You can change these at any point in time to invalidate all existing cookies. This will force all users to have to log in again.
 *
 * @since 2.6.0
 */
define('AUTH_KEY',         'jRF9N$#;n&7`LG}YBq;6L*TYG.cqj^;}Eogu=mfHR0_t?As:GI.84U56XiCS*c 6');
define('SECURE_AUTH_KEY',  '.)=/cMInm-<m$Pud-(WMNv>A {vTs-uRD+cQ*G:OP5B0-Ey?hn.|o6J&QD/xEA4:');
define('LOGGED_IN_KEY',    'PYJ^w `i/.fv.vyNYj/Xf^:uE1r!?ama[eO1MOYx+~==sf8dH%T?do(uEcw;1<S7');
define('NONCE_KEY',        'mlRCUv b^mb!+p;0Xk_/]G;/{QDPf(op:PVm&FG{y1> #/q{az82b2[Zp@7;Liyb');
define('AUTH_SALT',        'PJ%5pt;CJ ;%,_`{=@R~>x[gJT/b8<JteldT#OcweJ(2[jyl0?ADngfQPAk{I{AB');
define('SECURE_AUTH_SALT', '4f<jnsLa;i24R20Zr=<5DH)a%sr(E_!)5S5}Ey+O&U^.=T6zRN@6GvX?WraAJdY%');
define('LOGGED_IN_SALT',   'bpHsPUcu~p1di:rBFrGk+@dAG7Y_Bk#?%C!}o)mD,rf 5KK!~Cr~vfMk]<KAe<%<');
define('NONCE_SALT',       'bZ/T+mxFHf55V[VKE$#fz}a+Z+.S(DWM.$Ncay{~a=V_|gI@?Y+uC*@:aN:VdtSm');

/**#@-*/

/**
 * WordPress Database Table prefix.
 *
 * You can have multiple installations in one database if you give each
 * a unique prefix. Only numbers, letters, and underscores please!
 */
$table_prefix  = 'officewp_';

/**
 * For developers: WordPress debugging mode.
 *
 * Change this to true to enable the display of notices during development.
 * It is strongly recommended that plugin and theme developers use WP_DEBUG
 * in their development environments.
 *
 * For information on other constants that can be used for debugging,
 * visit the Codex.
 *
 * @link https://codex.wordpress.org/Debugging_in_WordPress
 */
define('WP_DEBUG', false);

/* That's all, stop editing! Happy blogging. */

/** Absolute path to the WordPress directory. */
if ( !defined('ABSPATH') )
	define('ABSPATH', dirname(__FILE__) . '/');

/** Sets up WordPress vars and included files. */
require_once(ABSPATH . 'wp-settings.php');
